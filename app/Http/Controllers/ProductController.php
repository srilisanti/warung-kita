<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Product;
use App\Http\Controllers\Controller;
use App\Models\Product as ModelsProduct;

class ProductController extends Controller
{
    function post(Request $request)
    {
        $product = new ModelsProduct;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->active = $request->active;
        $product->description = $request->description;

        $product->save();
        return response()->json([
            'message' => 'Success',
            "data" => $product
        ]);
    }


    function get()
    {
        $Productall = ModelsProduct::all();
        return response()->json(
            $Productall
        );
    }

    function getById($id)
    {
        $whereidproduct = ModelsProduct::where('id', $id)->get();
        return response()->json([
            'message' => 'Success',
            "data" => $whereidproduct
        ]);
    }

    function put($id, Request $request)
    {
        $productid = ModelsProduct::where('id', $id)->first();
        if ($productid) {
            $productid->name = $request->name ? $request->name : $productid->name;
            $productid->price = $request->price ? $request->price : $productid->price;
            $productid->quantity = $request->quantity ? $request->quantity : $productid->quantity;
            $productid->active = $request->active ? $request->active : $productid->active;
            $productid->description = $request->description ? $request->description : $productid->description;

            $productid->save();
            return response()->json([
                'message' => 'PUT Method Succes',
                "data" => $productid
            ]);
        }
        return response()->json(
            [
                'message' => 'Product with id ' . $id . " not found"
            ],
            400
        );
    }

    function delete($id)
    {
        $productid = ModelsProduct::where('id', $id)->first();
        if ($productid) {
            $productid->delete();
            return response()->json([
                'message' => 'DELETE product id ' . $id . "Success"
            ]);
        }
        return response()->json(
            [
                'message' => 'product with id ' . $id . " not found"
            ],
            400
        );
    }
}
